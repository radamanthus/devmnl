# DevMNL

## The Name

_Dev_ : Because it's of, and for, developers and devops professionals.

_MNL_ : Ideally we'd use PH, but we'd end up with DevPH which is uncomfortably similar to DevCon PH. As a proxy for PH, we use _MNL_ which is the code for the international airport that is the gateway to the country.

## What

DevMNL is an online community with (planned) regular meetups, targeted at senior-level software engineer and DevOps professionals. 

By seniors, for seniors.

Juniors are welcome to attend and join the discussions, but they are not the target of the content and activities.

### Meetup Topics / Online Discussion Topics

Here's the part of a typical career ladder that's relevant to a Senior Engineer (Senior Software Engineer or Senior SRE):

![Senior Engineer Career Path](senior-engineer-career-path.png)

Typical Senior Engineers divide their time between:
- mentoring junior/intermediate engineers in the team [4]
- performing engineering tasks as an individual contributor, with a much higher level of expectation for quality, security and scalability [1]
- growing their technical leadership [2]
- growing into a manager role [3]

We will attempt to cover all four areas in a similar manner, with a bigger emphasis on [1].

Below are some example talks we'd like to see under each category. 

[1] Technical
- [Database schema migrations with zero downtime](https://www.youtube.com/watch?v=ka-PLyjV3AI)
- [CRDTs: The Hard Parts](https://www.youtube.com/watch?v=x7drE24geUw)
- TODO: Add more examples

[2] Technical Leadership
- [The Lost Art Of Software Design](https://www.youtube.com/watch?v=36OTe7LNd6M)
- TODO: Add more examples

[3] Management
- TODO: Add more examples

[4] Mentoring
- [Code Review Best Practices For Software Engineers](https://www.youtube.com/watch?v=1Ge__2Yx_XQ)
- TODO: Add more examples

## Who

Senior-level software engineer and DevOps professionals, seeking senior-level discussions

## Why

Existing local tech communities are geared more towards expanding the community and not on nurturing/developing the skills of the existing community members.

Q: Everyone looks up to the Senior Engineer for professional development. Who does the Senior Engineer look up to?

Senior-level discussions are rare. Partly because seniors are a minority in many tech communities? To gather a critical mass of senior professionals, we cannot confine ourselves to a specific language, technology, or platform.

Although the community is technology-neutral, we strongly frown upon "best practices" discussions that are devoid of technology content. Discussions should be grounded on the day-to-day realities faced by an IT professional, not high-level concepts floating in the sky.

## Preferred Topics

- Architecture
- Deep dives
- Leadership

## Out Of Scope

We will not cover these topics: 

- Agile
- Design
- Product

## Similar Initiatives or Services

- [Top End Devs](https://topenddevs.com/). For paid subscribers only. They have career coaching, online meetups, and book clubs.
- Rails Performance Slack group - open only to people who bought Nate Berkopec's [The Complete Guide to Rails Performance](https://www.railsspeed.com/) and [Sidekiq in Practice](https://nateberk.gumroad.com/l/sidekiqinpractice)
